package com.test.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestApplication.class, args);
	}
	/* CRUD CREATE READ UPDATE DELETE
	@Controller pour les opérations du CRUD
	@Repository donne une interface en encapsulant la base de données et pouvoir la manipuler
	@Service pour des actions complexes
	*/ 

}
