package com.test.rest;

import java.util.List;
import java.util.Optional;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PutMapping;


@RestController
public class BookController {

    private final BookRepository bookRepository;

    public BookController(BookRepository bookRepository) {
      this.bookRepository = bookRepository;
    }

    @GetMapping("/books")
    public Iterable<Book> findAllBooks() {
      return this.bookRepository.findAll();
    }

    @GetMapping("/books/{id}")
    public Optional<Book> getBookById(@PathVariable("id") Long id) {
        return this.bookRepository.findById(id);
    }

    @GetMapping("/books/title/{title}")
    public List<Book> getBookByTitle(@PathVariable("title") String title) {
        return this.bookRepository.findByTitle(title);
    }

    @PostMapping("/books")
    public Book addOneBook(@RequestBody Book book) {
      return this.bookRepository.save(book);
    }

    @DeleteMapping("/books/{id}")
    public void deleteBookById(@PathVariable("id") Long id) {
        this.bookRepository.deleteById(id);
    }

    @PutMapping("/books/{id}")
    public Book updateBook(@PathVariable("id") Long id, @RequestBody Book updatedBookData) {
    Optional<Book> optionalBook = bookRepository.findById(id);

    if (optionalBook.isPresent()) {
        Book existingBook = optionalBook.get();
        existingBook.setTitle(updatedBookData.getTitle());
        existingBook.setDescription(updatedBookData.getDescription());
        existingBook.setAvailable(updatedBookData.isAvailable());
        final Book updatedBook = bookRepository.save(existingBook);
        return updatedBook;
    } else {
        throw new Error("Book not found with id: " + id);
    }
    }
}
