package com.test.rest;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "book")

public class Book {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="title", length=100, nullable = false)
    private String title;

    @Column(name="description", columnDefinition = "TEXT", nullable = true)
    private String description;

    @Column(name="available")
    private Boolean available = true;

    public Book() {}

    public Book(Long id, String title, String description, Boolean available) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.available = available;
      }
      
      public Long getId() {
        return this.id;
      }
      
      public String getTitle() {
        return this.title;
      }
    
      public String getDescription() {
        return this.description;
      }
    
      public Boolean isAvailable() {
        return this.available;
      }
      
      public void setTitle(String title) {
        this.title = title;
      }
    
      public void setDescription(String description) {
       this.description = description;
      }
    
      public void setAvailable(Boolean available) {
        this.available = available;
      }
}
